package pizzashop.food;

public interface OrderItem {
	public String toString();
	public double getPrice();
	public void setSize(int size);
}
