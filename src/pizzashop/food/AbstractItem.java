package pizzashop.food;

public abstract class AbstractItem implements OrderItem{

	public static final String [] sizes = { "None", "Small", "Medium", "Large" };
	protected int size;
	public AbstractItem(int size) {
		this.size = size;
		
	}
	@Override
	public abstract double getPrice();

	@Override
	public abstract void setSize(int size);
	
}
